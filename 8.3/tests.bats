#!/usr/bin/env bats

setup() {
  PROJECT_ROOT="$(cd "$(dirname "$BATS_TEST_FILENAME")/.." >/dev/null 2>&1 && pwd)"

  load "${PROJECT_ROOT}/test/test_helper/common-setup"
  _common_setup
}

## Image related tests

@test "the image has the main tags are created" {
  id=`bash -c "docker images registry.gitlab.com/stelgenhof/docker-php-nginx:php82 --format '{{.ID}}'"`
  run bash -c "docker images registry.gitlab.com/stelgenhof/docker-php-nginx --format '{{.ID}} ({{.Tag}})' | grep $id"

  assert_success

  assert_output --regexp '^[a-z0-9]{12}\s\(php82\)$'
}

## OS related tests

@test "the image has the correct alpine os version installed" {
  run docker run --rm ${container} awk -F= '$1=="VERSION_ID" { gsub(/"/, "", $2); print $2 ;}' /etc/os-release

  assert_success
  assert_output "3.20.0"
}

@test "the image has the correct composer version installed" {
  run bash -c "docker run --rm ${container} composer -V"

  version="$(echo $output | awk '{ print $3 ;}')"

  assert_success
  assert_equal "$version" "2.7.6"
}

## PHP related tests

@test "php version is correct" {
  run docker run --rm ${container} php -v

  version="$(echo $output | sed 's/.*PHP \([0-9].[0-9]\).*/\1/')"

  assert_success
  assert_equal $version "8.3"
}

@test "the image has the correct php modules installed" {
  run docker run --rm ${container} php -m

  assert_success
  assert_line 'bcmath'
  assert_line 'calendar'
  assert_line 'Core'
  assert_line 'ctype'
  assert_line 'curl'
  assert_line 'date'
  assert_line 'dom'
  assert_line 'fileinfo'
  assert_line 'filter'
  assert_line 'hash'
  assert_line 'iconv'
  assert_line 'intl'
  assert_line 'json'
  assert_line 'libxml'
  assert_line 'mbstring'
  assert_line 'mysqlnd'
  assert_line 'openssl'
  assert_line 'pcre'
  assert_line 'Phar'
  assert_line 'posix'
  assert_line 'readline'
  assert_line 'Reflection'
  assert_line 'session'
  # assert_line 'SimpleXML'
  assert_line 'SPL'
  assert_line 'standard'
  assert_line 'tokenizer'
  assert_line 'xml'
  assert_line 'xmlreader'
  assert_line 'xmlwriter'
  assert_line 'Zend OPcache'
  assert_line 'zip'
  assert_line 'zlib'
}

