_common_setup() {
  PROJECT_ROOT="$(cd "$(dirname "$BATS_TEST_FILENAME")/.." >/dev/null 2>&1 && pwd)"

  load "${PROJECT_ROOT}/test/test_helper/bats-support/load"
  load "${PROJECT_ROOT}/test/test_helper/bats-assert/load"
}
