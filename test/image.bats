#!/usr/bin/env bats

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "the image has an authors label" {
  run bash -c "docker inspect ${container} | jq -r '.[].Config.Labels.\"org.opencontainers.image.authors\"'"

  assert_success
  assert_output "Sacha Telgenhof <me@sachatelgenhof.com"
}

@test "the image has a title label" {
  run bash -c "docker inspect ${container} | jq -r '.[].Config.Labels.\"org.opencontainers.image.title\"'"

  assert_success
  assert_output "docker-php-nginx"
}

@test "the image has a description label" {
  run bash -c "docker inspect ${container} | jq -r '.[].Config.Labels.\"org.opencontainers.image.description\"'"

  assert_success
  assert_output "Lightweight container with latest NGINX & PHP-FPM based on Alpine Linux"
}

@test "the image has a repository url label" {
  run bash -c "docker inspect ${container} | jq -r '.[].Config.Labels.\"org.opencontainers.image.url\"'"

  assert_success
  assert_output "https://gitlab.com/stelgenhof/docker-php-nginx"
}

@test "the image has a vcs-ref label set to the current head commit" {
  run bash -c "docker inspect ${container} | jq -r '.[].Config.Labels.\"org.opencontainers.image.revision\"'"

  assert_success
  assert_output `git rev-parse --short HEAD`
}

@test "the image has a build-date label" {
  run bash -c "docker inspect ${container} | jq -r '.[].Config.Labels.\"org.opencontainers.image.created\"'"

  assert_success
  assert_output
}
