# Docker PHP NGINX

Lightweight container with latest NGINX & PHP-FPM based on Alpine Linux.

- Built on the lightweight and secure Alpine Linux distribution
- Very small Docker image size (less than 65MB)
- Uses PHP 7/8 for better performance, lower CPU usage & memory footprint
- Optimized for 100 concurrent users
- Optimized to only use resources when there's traffic (by using PHP-FPM's on-demand PM)
- The servers NGINX, PHP-FPM and `supervisord` run under a non-privileged user (nobody) to make it more secure
- The logs of all the services are redirected to the output of the Docker container
  (visible with `docker logs -f <container name>`).

## Usage

Start the Docker container:

    docker run -p 80:8080 registry.gitlab.com/stelgenhof/docker-php-nginx:<tag>

Or mount your own code to be served by PHP-FPM & NGINX

    docker run -p 80:8080 -v $(pwd):/var/www/html registry.gitlab.com/stelgenhof/docker-php-nginx:<tag>

## NGINX

Please note that NGINX is configured to serve the pages from `/var/www/html/public`. This is a common setup for popular
PHP frameworks such as Symfony and Laravel.

### Acknowledgements

Based on [https://github.com/TrafeX/docker-php-nginx](https://github.com/TrafeX/docker-php-nginx)
